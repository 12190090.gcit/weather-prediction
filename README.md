# Weather Forecast



## Gyalpozhing College of Information Technology (Bachelor of Science of Information Technology ) PRJ303
Weather prediction is a prediction of weather by using parameters such as precipitation, maximum temperature, minimum temperature and wind. The Ridge machine learning algorithm is used to forecast the weather. 

Visit our website : (https://weather-prediction-prj.herokuapp.com)

Built using: Python, HTML/CSS/JS, Flask

Promotional video : (https://www.youtube.com/watch?v=qdfX8yOJ0CM)

## Screenshot of Website 
<img src="static/home.png"/>
<img src="static/portfolio.png"/>
<img src="static/about.png"/>
<img src="static/team.png"/>
<img src="static/footer.png"/>

### PREDICT
Sample parameters 

fog( Parameters: 0, Maximum temperature: 1.1, Minimum temperature: -1.1, Wind: 0)

rain( Parameters: 3, Maximum temperature: 10.2, Minimum temperature: 3.7, Wind: 4)

snow( Parameters: 5.3, Maximum temperature: 1.1, Minimum temperature: -3.3, Wind: 2.2)

sum( Parameters: 0, Maximum temperature: 14.4, Minimum temperature: 2.2, Wind: 5.3)



<img src="static/predict.png"/>



## Logo
<img src="static/logo.png"/>

##  Final Poster
<img src="static/poster.png"/>

